package org.rabilint.bsagiphy.repository;

import com.opencsv.CSVReader;
import org.rabilint.bsagiphy.config.UserProperties;
import org.rabilint.bsagiphy.entity.Gifs;
import org.rabilint.bsagiphy.entity.Query;
import org.rabilint.bsagiphy.entity.UserHistory;
import org.rabilint.bsagiphy.util.StorageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository {

    private static final Logger logger = LoggerFactory.getLogger(UserRepository.class);

    @Autowired
    UserProperties userProperties;

    public List<Gifs> getUserFileList(String id) throws IOException {
        ArrayList<Gifs> gifs = new ArrayList<>();
        Path rootPath = Paths.get(userProperties.getRootPath() + "//" + id);
        for (String directory : StorageUtils.scanDirectoryPath(rootPath)) {
            if (directory.isEmpty()) continue;
            String tag = directory.substring(1);
            gifs.add(new Gifs(tag, StorageUtils.scanDirectoryPathByTag(tag, rootPath)));
        }
        return gifs;
    }

    public List<UserHistory> getUserHistory(String id) {

        String historyFile = getHistoryFile(id);
        ArrayList<UserHistory> userHistories = new ArrayList<>();
        CSVReader reader;
        try {
            reader = new CSVReader(new FileReader(historyFile));
            String[] line;
            while ((line = reader.readNext()) != null) {
                userHistories.add(new UserHistory(line[0], line[1], line[2]));
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userHistories;
    }

    private String getHistoryFile(String id) {
        return userProperties.getRootPath() + "//" + id + "//" + userProperties.getHistoryFileName();
    }

    public void clearUserHistory(String id) {
        try {
            Files.deleteIfExists(Paths.get(getHistoryFile(id)));
        } catch (NoSuchFileException e) {
            logger.error("No such file/directory exists");
        } catch (DirectoryNotEmptyException e) {
            logger.error("Directory is not empty.");
        } catch (IOException e) {
            logger.error("Invalid permissions.");
        }
        logger.error("Deletion successful.");
    }

    public String generateGiphy(String id, Query query) {

        return null;
    }
}
