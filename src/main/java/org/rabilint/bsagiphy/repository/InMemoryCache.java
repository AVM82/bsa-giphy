package org.rabilint.bsagiphy.repository;

import org.rabilint.bsagiphy.entity.Gifs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class InMemoryCache {

    private static InMemoryCache instance;
    private Map<String, List<Gifs>> inMemoryCache = new HashMap<>();

    private static final Logger logger = LoggerFactory.getLogger(InMemoryCache.class);

    public static synchronized InMemoryCache getInstance() {
        if (instance == null) {
            instance = new InMemoryCache();
        }
        return instance;
    }

    public void putToCache(String userId, String tag, String path) {
        List<Gifs> listGifs = inMemoryCache.get(userId);
        LinkedList<String> list = new LinkedList<>();
        list.add(path);
        if (listGifs == null) {
            LinkedList<Gifs> gifs = new LinkedList<>();
            gifs.add(new Gifs(tag, list));
            inMemoryCache.put(userId, gifs);
        } else {
            int index = findGifs(tag, listGifs);
            if (index != -1) {
                inMemoryCache.get(userId).get(index).getGifs().add(path);
            } else {
                inMemoryCache.get(userId).add(new Gifs(tag, list));
            }
        }
        logger.info(inMemoryCache.toString());
    }

    private int findGifs(String tag, List<Gifs> listGifs) {
        int index = -1;
        for (Gifs item : listGifs) {
            if (item.getQuery().equals(tag)) {
                index++;
                return index;
            }
        }
        return index;
    }

    public void deleteUser(String userId) {
        inMemoryCache.remove(userId);
    }
}
