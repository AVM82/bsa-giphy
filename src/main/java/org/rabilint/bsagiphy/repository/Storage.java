package org.rabilint.bsagiphy.repository;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.rabilint.bsagiphy.config.StorageProperties;
import org.rabilint.bsagiphy.config.UserProperties;
import org.rabilint.bsagiphy.entity.Gifs;
import org.rabilint.bsagiphy.util.StorageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class Storage {

    private static final Logger logger = LoggerFactory.getLogger(Storage.class);

    @Autowired
    StorageProperties storageProperties;

    @Autowired
    UserProperties userProperties;

    public List<Gifs> getGifs(String query) throws IOException {
        ArrayList<Gifs> gifs = new ArrayList<>();
        Path rootPath = Paths.get(storageProperties.getRootPath());
        if (query == null) {
            List<String> directories = StorageUtils.scanDirectoryPath(rootPath);
            for (String name : directories) {
                if (name.isEmpty()) continue;
                String tag = name.substring(1);
                gifs.add(new Gifs(tag, StorageUtils.scanDirectoryPathByTag(tag, rootPath)));
            }
        } else {
            gifs.add(new Gifs(query, StorageUtils.scanDirectoryPathByTag(query, rootPath)));
        }
        return gifs;
    }

    public List<String> getAllCachedGifs() throws IOException {
        Path rootPath = Paths.get(storageProperties.getRootPath());
        try (Stream<Path> stream = Files.walk(rootPath, Integer.MAX_VALUE)) {
            return stream
                    .filter(Files::isRegularFile)
                    .map(item -> item.toString().substring(rootPath.toString().length()))
                    .sorted()
                    .collect(Collectors.toList());
        }
    }

    public String findGiphy(String query) {
        try {
            List<String> gifs = StorageUtils.scanDirectoryPathByTag(query, Paths.get(storageProperties.getRootPath()));
            return gifs.get(StorageUtils.randomizer(gifs.size()));
        } catch (IOException e) {
            logger.error(e.getMessage());
            return "";
        }
    }

    public void deleteUserCache(String id) {
        try {
            FileUtils.deleteDirectory(new File(userProperties.getRootPath() + "//" + id));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
