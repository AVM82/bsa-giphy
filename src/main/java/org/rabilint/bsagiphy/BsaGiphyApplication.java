package org.rabilint.bsagiphy;

import org.rabilint.bsagiphy.config.StorageProperties;
import org.rabilint.bsagiphy.config.UserProperties;
import org.rabilint.bsagiphy.util.StorageUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@ConfigurationPropertiesScan
@EnableConfigurationProperties({StorageProperties.class, UserProperties.class})
public class BsaGiphyApplication {

    public static void main(String[] args) {
        SpringApplication.run(BsaGiphyApplication.class, args);

        StorageUtils.makeWorkflow();

    }
}
