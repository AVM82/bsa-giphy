package org.rabilint.bsagiphy.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "giphy")
@Getter
@Setter
public class GiphyProperties {
    private String url;
    private String schema;
}
