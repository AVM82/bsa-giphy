package org.rabilint.bsagiphy.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ApiKeyGiphyConfigProd implements ApiKeyGiphyConfig{

    @Override
    public String getApiKey() {
        return "";
    }
}
