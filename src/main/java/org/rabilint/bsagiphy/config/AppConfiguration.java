package org.rabilint.bsagiphy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import java.net.http.HttpClient;

@Configuration
public class AppConfiguration {

    @Bean
    @Scope(value = "prototype")
    public HttpClient httpClient() {
        return HttpClient.newHttpClient();
    }

    @Bean(name = "dev")
    public ApiKeyGiphyConfigDev apiKeyGiphyConfigDev() {
        return new ApiKeyGiphyConfigDev();
    }

    @Bean(name = "prod")
    public ApiKeyGiphyConfigProd apiKeyGiphyConfigProd() {
        return new ApiKeyGiphyConfigProd();
    }

    @Bean
    public Giphy giphy(GiphyProperties giphyProperties) {
        return new Giphy(
                giphyProperties.getUrl(),
                giphyProperties.getSchema()
        );
    }
}
