package org.rabilint.bsagiphy.config;

import lombok.Getter;

@Getter
public class Giphy {

    private final String url;
    private final String schema;

    public Giphy(String url, String schema) {
        this.url = url;
        this.schema = schema;
    }
}
