package org.rabilint.bsagiphy.config;

public interface ApiKeyGiphyConfig {

   String getApiKey();
}
