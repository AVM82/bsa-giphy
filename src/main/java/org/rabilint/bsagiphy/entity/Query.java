package org.rabilint.bsagiphy.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public final class Query {

    private String query;
    private boolean force;

    public Query() {
    }
}
