package org.rabilint.bsagiphy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserHistory {
    private String date;
    private String query;
    private String gif;
}
