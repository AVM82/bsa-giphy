package org.rabilint.bsagiphy.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public final class Gifs {
    private final String query;
    private List<String> gifs;

}
