package org.rabilint.bsagiphy.service;

import org.rabilint.bsagiphy.config.StorageProperties;
import org.rabilint.bsagiphy.entity.Gifs;
import org.rabilint.bsagiphy.repository.Storage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.apache.catalina.startup.ExpandWar.deleteDir;

@Service
public class StorageService {

    @Autowired
    Storage storage;

    @Autowired
    StorageProperties storageProperties;

    public List<Gifs> getDiskCache(String query) throws IOException {
        return storage.getGifs(query);
    }

    public void deleteCache() {
        String path = storageProperties.getRootPath();
        File rootDir = new File(path);
        File[] files = rootDir.listFiles();
        if (files != null) {
            for (final File file : files) {
                deleteDir(file);
            }
        }
    }

    public List<String> getAllCachedGifs() throws IOException {
        return storage.getAllCachedGifs();
    }
}
