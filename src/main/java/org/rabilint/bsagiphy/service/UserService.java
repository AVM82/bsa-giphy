package org.rabilint.bsagiphy.service;

import org.rabilint.bsagiphy.config.StorageProperties;
import org.rabilint.bsagiphy.config.UserProperties;
import org.rabilint.bsagiphy.entity.Gifs;
import org.rabilint.bsagiphy.entity.Query;
import org.rabilint.bsagiphy.entity.UserHistory;
import org.rabilint.bsagiphy.repository.InMemoryCache;
import org.rabilint.bsagiphy.repository.Storage;
import org.rabilint.bsagiphy.repository.UserRepository;
import org.rabilint.bsagiphy.util.StorageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    Storage storage;

    @Autowired
    StorageProperties storageProperties;

    @Autowired
    UserProperties userProperties;

    @Autowired
    HttpGiphyClient httpGiphyClient;

    public List<Gifs> getUserFileList(String id) throws IOException {
        return userRepository.getUserFileList(id);
    }

    public List<UserHistory> getUserHistory(String id) {
        return userRepository.getUserHistory(id);
    }

    public void clearUserHistory(String id) {
        userRepository.clearUserHistory(id);
    }

    public String generateGiphy(String id, Query query) {
        String gif = "";
        if (!query.isForce()) {
            gif = storage.findGiphy(query.getQuery());
        }
        if (gif.isEmpty()) {
            gif = httpGiphyClient.generate(query);
        }
        String source = storageProperties.getRootPath() + gif;
        String destination = userProperties.getRootPath() + "//" + id + gif;
        String userDir = StorageUtils.makeUserDir(userProperties.getRootPath(), id);
        StorageUtils.makeUserTagDir(userDir, query.getQuery());
        StorageUtils.copyFromCache(source, destination);
        InMemoryCache.getInstance().putToCache(id, query.getQuery(), gif);

        String line = buildHistory(query.getQuery(), gif);
        writeHistory(id, line);
        return gif;
    }

    private String buildHistory(String query, String gif) {
        String line = "";
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date(System.currentTimeMillis());
        line += formatter.format(date);
        line += ",";
        line += query;
        line += ",";
        line += gif;
        line += System.lineSeparator();
        return line;
    }

    private void writeHistory(String userId, String line) {
        try (PrintWriter pw = new PrintWriter(
                new FileWriter(userProperties.getRootPath() + "//" + userId +
                        "//" + userProperties.getHistoryFileName()))) {
            pw.write(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteUserCache(String id) {
        InMemoryCache.getInstance().deleteUser(id);
        storage.deleteUserCache(id);
    }
}
