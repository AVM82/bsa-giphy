package org.rabilint.bsagiphy.service;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.rabilint.bsagiphy.config.ApiKeyGiphyConfig;
import org.rabilint.bsagiphy.config.Giphy;
import org.rabilint.bsagiphy.config.StorageProperties;
import org.rabilint.bsagiphy.entity.Query;
import org.rabilint.bsagiphy.util.StorageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Component
public class HttpGiphyClient {

    @Qualifier(value = "dev")
    @Autowired
    ApiKeyGiphyConfig apiKeyGiphyConfig;

    @Autowired
    Giphy giphy;

    @Autowired
    StorageProperties storageProperties;

    private static final Logger logger = LoggerFactory.getLogger(HttpGiphyClient.class);

    private final HttpClient client;

    @Autowired
    public HttpGiphyClient(HttpClient client) {
        this.client = client;
    }

    private String generateGiphy(String query) throws JSONException, IOException {

        JSONObject jsonObject = new JSONObject(getGiphyAsJson(query));
        String giphyId = jsonObject.getJSONObject("data").getString("id");
        String giphyUrl = jsonObject.getJSONObject("data")
                .getString("image_original_url");
        logger.info("Giphy id -> " + giphyId);
        logger.info("Giphy URL -> " + giphyUrl);

        return saveGiphy(giphyUrl, giphyId, query);

    }

    private String saveGiphy(String giphyUrl, String giphyId, String query) throws IOException {
        CloseableHttpClient giphyClient = HttpClientBuilder.create().build();
        HttpGet giphyRequest = new HttpGet(giphyUrl);
        String path = storageProperties.getRootPath() + "//" + query;
        String fileName = path + "//" + giphyId + ".gif";
        try (CloseableHttpResponse response = giphyClient.execute(giphyRequest)) {
            HttpEntity entity = response.getEntity();
            File directory = new File(path);
            if (StorageUtils.isDirectoryNotExist(directory)) {
                if (directory.mkdir()) {
                    logger.info("Successfully created new directory : " + path);
                } else {
                    logger.info("Failed to create new directory: " + path);
                }
            }

            try (InputStream inputStream = entity.getContent();
                 FileOutputStream fos = new FileOutputStream(fileName)) {
                int b;
                while ((b = inputStream.read()) != -1) {
                    fos.write(b);
                }
            }
        }
        return "//" + query + "//" + giphyId + ".gif";
    }

    private String getGiphyAsJson(String query) {
        try {
            var response = client.send(buildGetRequest(query), HttpResponse.BodyHandlers.ofString());
            logger.info("Response body: " + response.body());
            return response.body();
        } catch (IOException | InterruptedException | URISyntaxException ex) {
            logger.error(ex.getMessage(), ex);
            return "";
        }
    }

    private HttpRequest buildGetRequest(String query) throws URISyntaxException {
        return HttpRequest
                .newBuilder()
                .uri(getUri(query))
                .GET()
                .build();
    }

    private URI getUri(String query) throws URISyntaxException {
        return new URIBuilder()
                .setScheme(giphy.getSchema())
                .setHost(giphy.getUrl())
                .setParameter("api_key", apiKeyGiphyConfig.getApiKey())
                .setParameter("tag", query)
                .build();
    }

    public String generate(Query query) {
        try {
            return generateGiphy(query.getQuery());
        } catch (JSONException | IOException e) {
            logger.error(e.getMessage());
            return "";
        }
    }
}
