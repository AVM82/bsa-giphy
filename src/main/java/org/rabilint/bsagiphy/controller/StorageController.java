package org.rabilint.bsagiphy.controller;

import org.rabilint.bsagiphy.entity.Gifs;
import org.rabilint.bsagiphy.entity.Query;
import org.rabilint.bsagiphy.service.HttpGiphyClient;
import org.rabilint.bsagiphy.service.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
public final class StorageController {

    private static final Logger logger = LoggerFactory.getLogger(StorageController.class);

    @Autowired
    StorageService storageService;

    @Autowired
    HttpGiphyClient httpGiphyClient;

    @GetMapping("/cache")
    public ResponseEntity<List<Gifs>> getDiskCache(@RequestParam(required = false) String query) {
        logger.debug("Method getDiskCache was called");
        try {
            return ResponseEntity.status(HttpStatus.OK).body(storageService.getDiskCache(query));
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ArrayList<>());
        }
    }

    @PostMapping("/cache/generate")
    public List<Gifs> generate(@RequestBody Query query) {
        logger.debug("Method generate was called");
        try {
            httpGiphyClient.generate(query);
            return storageService.getDiskCache(query.getQuery());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @DeleteMapping("/cache")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteCache() {
        logger.info("Method deleteCache was called");
        storageService.deleteCache();
    }

    @GetMapping("/cache/gifs")
    public List<String> getAllCachedGifs() {
        logger.info("Method getAllCachedGifs was called");
        try {
            return storageService.getAllCachedGifs();
        } catch (IOException e) {
            logger.error(e.getMessage());
            return null;
        }
    }
}
