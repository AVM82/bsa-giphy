package org.rabilint.bsagiphy.controller;

import org.rabilint.bsagiphy.entity.Gifs;
import org.rabilint.bsagiphy.entity.Query;
import org.rabilint.bsagiphy.entity.UserHistory;
import org.rabilint.bsagiphy.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/user")
public final class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @GetMapping("/{id}/all")
    public List<Gifs> getUserFileList(@PathVariable String id) {
        List<Gifs> gifs = null;
        logger.info("Method getUserFileList was called");
        try {
            gifs = userService.getUserFileList(id);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return gifs;
    }

    @GetMapping("/{id}/history")
    public List<UserHistory> getUserHistory(@PathVariable String id) {
        logger.info("Method getUserHistory was called");
        return userService.getUserHistory(id);
    }

    @DeleteMapping("/{id}/history/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void clearUserHistory(@PathVariable String id) {
        logger.info("Method clearUserHistory was called");
        userService.clearUserHistory(id);
    }

    @PostMapping("/{id}/generate")
    public String generateGiphy(@PathVariable String id, @RequestBody Query query) {
        logger.info("Method generateGiphy was called");
        return userService.generateGiphy(id, query);
    }

    @DeleteMapping("/{id}/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUserCache(@PathVariable String id) {
        userService.deleteUserCache(id);
    }
}
