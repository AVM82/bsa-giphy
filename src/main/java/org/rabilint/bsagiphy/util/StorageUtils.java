package org.rabilint.bsagiphy.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class StorageUtils {

    private static final Logger logger = LoggerFactory.getLogger(StorageUtils.class);

    public static List<String> scanDirectoryPath(Path rootPath) throws IOException {
        try (Stream<Path> stream = Files.walk(rootPath, Integer.MAX_VALUE)) {
            return stream
                    .filter(Files::isDirectory)
                    .map(item -> item.toString().substring(rootPath.toString().length()))
                    .sorted()
                    .collect(Collectors.toList());
        }
    }

    public static List<String> scanDirectoryPathByTag(String tag, Path rootPath) throws IOException {
        Path tagPath = Paths.get(rootPath + "//" + tag);
        try (Stream<Path> stream = Files.walk(tagPath, Integer.MAX_VALUE)) {
            return stream
                    .filter(Files::isRegularFile)
                    .map(item -> item.toString().substring(rootPath.toString().length()))
                    .sorted()
                    .collect(Collectors.toList());
        }
    }

    /*
    return random value with upperbound equals value
     */
    public static int randomizer(int value) {
        Random rand = new Random();
        return rand.nextInt(value);
    }

    public static void copyFromCache(String source, String destination) {
        FileChannel sourceChannel = null;
        FileChannel destChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destChannel = new FileOutputStream(destination).getChannel();
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                sourceChannel.close();
                destChannel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String makeUserDir(String rootPath, String id) {
        File directory = new File(rootPath + "//" + id);
        return makeDir(directory);

    }

    public static String makeUserTagDir(String userPath, String tag) {
        File directory = new File(userPath + "//" + tag);
        return makeDir(directory);

    }

    private static String makeDir(File directory) {
        if (StorageUtils.isDirectoryNotExist(directory)) {
            if (directory.mkdir()) {
                logger.info("Successfully created new directory : " + directory);
            } else {
                logger.info("Failed to create new directory: " + directory);
            }
        }
        return directory.getPath();
    }

    public static boolean isDirectoryNotExist(File directory) {
        if (directory.exists()) {
            logger.info("Directory already exists");
            return false;
        } else {
            logger.info("Directory not exists");
            return true;
        }
    }

    public static void makeWorkflow (){
        File directory = new File("bsa_giphy");
        makeDir(directory);
        directory = new File("bsa_giphy//cache");
        makeDir(directory);
        directory = new File("bsa_giphy//users");
        makeDir(directory);
    }
}
